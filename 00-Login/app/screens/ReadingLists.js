import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { ListItem } from 'react-native-elements';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

function ReadingLists() {
  const list = [
    {
      title: 'Currently Reading',
      icon: 'book-reader'
    },
    {
      title: 'Want to Read',
      icon: 'book-open'
    },
    {
      title: 'Read',
      icon: 'book'
    }
  ]
    return (
        <View style={styles.container}>
          <Text style={styles.title}>
            Reading Lists
          </Text>
          {
            list.map((item, i) => (
              <ListItem
                key={i}
                title={item.title}
                leftIcon={<FontAwesomeIcon icon={item.icon}/>}
                bottomDivider
                chevron
              />
            ))
          }
        </View>
      );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});

export default ReadingLists;