import React, {useState} from 'react';
import {Alert, Button, StyleSheet, Text, View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Auth0 from 'react-native-auth0';

var credentials = require('./../auth0-configuration');
const auth0 = new Auth0(credentials);

const SignInScreen = props => {
  const [accessToken, setAccessToken] = useState(null);
  const [idToken, setIdToken] =  useState(null);

  const _onLogin = async () => {
    auth0.webAuth
      .authorize({
        scope: 'openid profile email',
        prompt: 'select_account'
      })
      .then(async credentials => {
        await AsyncStorage.setItem('userToken', `${credentials.accessToken}`);
        await AsyncStorage.setItem('idToken', `${credentials.idToken}`);
        setAccessToken(credentials.accessToken);
        setIdToken(credentials.idToken);
      })
      .then(() => {
        props.navigation.navigate('App');
      })
      .catch(error => console.log(error));
  };

  const _onLogout = async () => {
    auth0.webAuth
      .clearSession({})
      .then(success => {
        setAccessToken(null);
      })
      .then(async () => {
        await AsyncStorage.clear();
        props.navigation.navigate('Auth');
      })
      .catch(error => {
        console.log('Log out cancelled');
      });
  };

  let loggedIn = accessToken === null ? false : true;
  return (
    <View style={styles.container}>
      <Text style={styles.header}> Welcome to BookShelf </Text>
      <Button
        onPress={loggedIn ? _onLogout : _onLogin}
        title={loggedIn ? 'Log Out' : 'Log In'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default SignInScreen;
