import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, Image, View, ScrollView, RefreshControl} from 'react-native';
import { Card, ListItem, Button, Icon, SearchBar } from 'react-native-elements';
import BookCard from '../components/BookCard';
import {REACT_APP_BASE_URL} from 'react-native-dotenv';

function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

function Bookcase(props) {
  const [books, setBooks] =  useState([]);
  const [images, setImages]= useState([]);
  const [search, setSearch]=useState('');
  const [searchList, setSearchList] = useState([]);
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getBooks();
    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  const getBooks = async () => {
    const response = await fetch(`http://${REACT_APP_BASE_URL}:8080/book`, {
        method: 'GET'});
    const items = await response.json();
    const bks=[];
    items.map(x => {bks.push(x)})
    setBooks(bks);
    console.log(`http://${REACT_APP_BASE_URL}/book`)
  }

  useEffect(()=>{
    getBooks();
  }, []);

  useEffect(() => {
  }, [search]);

  function wait(timeout) {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }
  

  async function findBookImages() {
    const result = await fetch(`https://www.googleapis.com/books/v1/volumes?q=${search}`);
    const items = await result.json();
    const things = items.items;
    const imgs=[];
    things.map(x => {imgs.push(x.volumeInfo.imageLinks.smallThumbnail);})
    setImages(imgs);
    console.debug(images);
  }

  return (
    <ScrollView
    refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
    <Text style={styles.title}>BookCase</Text>
    <View style={styles.container}>
      {books.map((i, x) => {return <BookCard title={i.title} author={i.author} key={x} style={styles.Card}/>})}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    alignContent: 'center',
    flexWrap: 'wrap',
    backgroundColor: '#F5FCFF',
  },
  Card: {
    width: 100,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default Bookcase;
