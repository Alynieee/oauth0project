import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import { Card, ListItem, Button, Icon, SearchBar } from 'react-native-elements';
import BookCard from '../components/BookCard';

function Explore(props) {
  const [images, setImages]= useState([]);
  const [search, setSearch]=useState('');

  useEffect(() => {}, [search, images]);

  async function findBookImages() {
    const result = await fetch(`https://www.googleapis.com/books/v1/volumes?q=${search}`);
    const items = await result.json();
    const books = items.items;
    const imgs=[];
    books.map(x => {imgs.push(x.volumeInfo.imageLinks.smallThumbnail);})
    setImages(imgs);
    console.debug(images);
  }

  return (
    <ScrollView>
    <SearchBar
            placeholder="Search"
            onChangeText={search => setSearch(search)}
            value={search}
            round={true}
            containerStyle={styles.searchBar}
          />
    <Button title="Find Books" onPress={findBookImages}/>
    <Text style={styles.title}>Explore</Text>
    <View style={styles.container}>
      {images.map((i, x) => {return <Image style={{width: 100, height: 150, margin: 10}} source={{uri: i}} key={x}/>})}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  searchBar: {
    flex: 1,
    justifyContent: 'center',
    ...Platform.select({
      ios: {
        paddingTop: 50,
      },
      android: {
        paddingTop: 0,
      },
    }),
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexWrap: 'wrap',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});

export default Explore;