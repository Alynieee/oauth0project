import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, Text, ScrollView, View, Button, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Auth0 from 'react-native-auth0';
import { getStatusBarHeight } from 'react-native-safe-area-view';

var credentials = require('../auth0-configuration');
const auth0 = new Auth0(credentials);

function Profile(props) {
  const [user, setUser] = useState({name: "you"});
  const [accessToken, setAccessToken] = useState("");
  const [idToken, setIdToken] = useState("");
  const onLogout = async () => {
    auth0.webAuth
      .clearSession({})
      .then(success => {
        console.log("Success");
      })
      .then(async () => {
        await AsyncStorage.clear();
        props.navigation.navigate('Auth');
      })
      .catch(error => {
        console.log('Log out cancelled' + error);
      });
  };

  const getUser = async () => {
    var token = await AsyncStorage.getItem('userToken');
    var jwt = await AsyncStorage.getItem('idToken');
    setAccessToken(token);
    setIdToken(jwt);
    let response = await fetch("https://alynie.auth0.com/userinfo", {
    method: 'GET',
    headers: {
        'Authorization': `Bearer ${token}`
      }
    });
    let result = await response.json();
    console.log(jwt);
    setUser(result);
  }

  useEffect(() => {
    getUser();
  }, [])

  return (
    <ScrollView style={styles.container}>
          <View style={styles.header}></View>
          <Image style={styles.avatar} source={{uri: user.picture}}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>{user.name}</Text>
              <Text style={styles.info}>{user.email} {user.email_verified ? `(verified)` : `(not verified)`}</Text>
              <Text style={styles.descriptionTitle}>ID:</Text>
              <Text style={styles.description}>{user.sub}</Text>
              <Text style={styles.descriptionTitle}>AccessToken:</Text>
              <Text style={styles.description}>{accessToken}</Text>
              <Text style={styles.descriptionTitle}>IdToken:</Text>
              <Text style={styles.description}>{idToken}</Text>
              <TouchableOpacity style={styles.buttonContainer} onPress={() => {
                onLogout();
              }}>
                <Text>Sign Out</Text>  
              </TouchableOpacity>              
            </View>
        </View>
      </ScrollView>
  );
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#00BFFF",
    height:200,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:130
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  body:{
    marginTop:40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:30,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#00BFFF",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  descriptionTitle:{
    fontSize:16,
    fontWeight: 'bold',
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop:40,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
});

export default Profile;
