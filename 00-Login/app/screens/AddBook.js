import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';
import { Input } from 'react-native-elements';
import {REACT_APP_BASE_URL} from 'react-native-dotenv';

function AddBook(props) {
  const [title, setTitle] = useState('');
  const [author, setAuthor] =  useState('');
  const [notes, setNotes] =  useState('');

  useEffect(()=>{
  }, [title, author, notes])

  const formHandler = async () => {
    let body = {
      "userId": null,
      "title": title,
      "author": author,
      "image": null,
      "dateFinished": null,
      "summary": notes
    };
    try {
      const response = await fetch(`http://${REACT_APP_BASE_URL}:8080/book`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type':'application/json'
        },
        body: JSON.stringify(body)
      });
      if (!response.ok) {
        throw new Error('Network response was not ok.');
      }
      props.navigation.navigate('Bookcase');
    } catch (error) {
      console.log('There has been a problem with your fetch operation: ', error.message);
    }
  };

    return (
        <View style={styles.container}>
          <Text style={styles.title}>
            AddBook
          </Text>
          <Input
            label="Title"
            placeholder="Title"
            onChangeText={(text => setTitle(text))}
          />
          <Input
            label="Author"
            placeholder="Author"
            onChangeText={(text => setAuthor(text))}
          />
          <Input
            label="Notes"
            multiline
            numberOfLines={5}
            inputStyle=""
            placeholder="Notes"
            onChangeText={(text => setNotes(text))}
          />
          <Button title="Submit" onPress={async () => await formHandler()}/>
        </View>
      );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});

export default AddBook;