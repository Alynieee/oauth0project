/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {Alert, Button, StyleSheet, Text, View} from 'react-native';
import Auth0 from 'react-native-auth0';
import AppContainer from './router';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee, faBook, faCompass, faPlusCircle, faList, faUser, faBookReader, faBookOpen } from '@fortawesome/free-solid-svg-icons'

library.add(fab, faCheckSquare, faCoffee, faBook, faCompass, faPlusCircle, faList, faUser, faBookReader, faBookOpen);

var credentials = require('./auth0-configuration');
const auth0 = new Auth0(credentials);

const App = () => {
  return <AppContainer />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default App;
