import React, {useState} from 'react';
import {StyleSheet, Text, Image, View, ScrollView} from 'react-native';
import { Card, ListItem, Button, Icon } from 'react-native-elements';

const BookCard = (props) => {

    return(
        <Card
            containerStyle={{width: 100, height: 150}}
        >
        <Text>{props.title}</Text>
        <Text>by: {props.author}</Text>

        </Card>
    );
};

export default BookCard;