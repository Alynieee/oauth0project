import React, {useState} from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Bookcase from './screens/Bookcase';
import Explore from './screens/Explore';
import AddBook from './screens/AddBook';
import ReadingLists from './screens/ReadingLists';
import Profile from './screens/Profile';
import SignInScreen from './screens/SignInScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const AppStack = createBottomTabNavigator({
  Bookcase: {
      screen: Bookcase, 
      navigationOptions: {
          tabBarLabel: 'Bookcase', 
          tabBarIcon: ({ tintColor }) => (
              <FontAwesomeIcon icon="book"/>
          )
      }
  },
  Explore: {
    screen: Explore, 
    navigationOptions: {
        tabBarLabel: 'Explore', 
        tabBarIcon: ({ tintColor }) => (
            <FontAwesomeIcon icon="compass"/>
        )
    }
  },
  AddBook: {
    screen: AddBook, 
    navigationOptions: {
        tabBarLabel: 'AddBook', 
        tabBarIcon: ({ tintColor }) => (
            <FontAwesomeIcon icon="plus-circle"/>
        )
    }
  },
  ReadingLists: {
    screen: ReadingLists, 
    navigationOptions: {
        tabBarLabel: 'ReadingLists', 
        tabBarIcon: ({ tintColor }) => (
            <FontAwesomeIcon icon="list"/>
        )
    }
  },
  Profile: {
    screen: Profile, 
    navigationOptions: {
        tabBarLabel: 'Profile', 
        tabBarIcon: ({ tintColor }) => (
            <FontAwesomeIcon icon="user"/>
        )
    }
  }

});
const AuthStack = createStackNavigator({ SignIn: SignInScreen });

export default AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);